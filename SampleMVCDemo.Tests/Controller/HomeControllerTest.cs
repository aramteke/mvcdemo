﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SampleMVCDemo.Controllers;
using System.Web.Mvc;
using SampleMVCDemo.Models;

namespace SampleMVCDemo.Tests.Controller
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.AreEqual("Sample fizz Buzz Application", result.ViewBag.Message);
        }

        [TestMethod]
        public void InvalidInputTest()
        {
            // Arrange
            HomeController controller = new HomeController();
            controller.ModelState.AddModelError("test", "test");
            // Act
            SampleModel sample = new SampleModel();
            sample.number = -123;
            
            ViewResult result = controller.Index(sample) as ViewResult;

            // Assert
            Assert.AreEqual("Invalid Input", result.ViewBag.Message);
        }

        [TestMethod]
        public void ValidInputTest()
        {
            // Arrange
            HomeController controller = new HomeController();
            
            // Act
            SampleModel sample = new SampleModel();
            sample.number = 22;

            ViewResult result = controller.Index(sample) as ViewResult;

            // Assert
            Assert.AreEqual("Number Entered is: 22", result.ViewBag.Message);
        }

        [TestMethod]
        public void ValidInputTestData()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            SampleModel sample = new SampleModel();
            sample.number = 3;

            ViewResult result = controller.Index(sample) as ViewResult;
            List<string> li = new List<string>();
            li.Add("1");
            li.Add("2");
            li.Add("fizz");

            // Assert
            
            CollectionAssert.AreEqual(li, result.ViewBag.list);
        }

        [TestMethod]
        public void Validatestringifdayiswednesday()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            SampleModel sample = new SampleModel();
            sample.number = 5;

            ViewResult result = controller.Index(sample) as ViewResult;
            List<string> li = new List<string>();
            if (DateTime.Now.DayOfWeek.ToString() == "Wednesday")
            {
                li.Add("1");
                li.Add("2");
                li.Add("wizz");
                li.Add("4");
                li.Add("wuzz");
            }
            else
            {
                li.Add("1");
                li.Add("2");
                li.Add("fizz");
                li.Add("4");
                li.Add("fuzz");
            }
            // Assert
            CollectionAssert.AreEqual(li, result.ViewBag.list);
        }
    }
}
