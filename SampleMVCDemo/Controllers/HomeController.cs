﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SampleMVCDemo.Models;

namespace SampleMVCDemo.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Message = "Sample fizz Buzz Application";
            return View();
        }


        [HttpPost]
        public ActionResult Index(SampleModel sample)
        {
            if (ModelState.IsValid)
            {
                ViewBag.Message = "Number Entered is: " + sample.number;
                //ViewBag.dayofweek = getdayofweek();
                ViewBag.list = getlist(sample.number, getdayofweek()); ;
                return View("DisplayData", sample);
            }
            else
            {
                ViewBag.Message = "Invalid Input";
                return View();
            }
        }

        public List<string> getlist(int num, string day)
        {
            List<string> str = new List<string>();
            for (int i = 1; i <= num; i++)
            {
                if (i % 3 == 0 && i % 5 == 0)
                {
                   str.Add("fizzbuzz");
                }
                else if (i % 3 == 0)
                {
                    if (day.Equals("Wednesday"))
                    {
                        str.Add("wizz");
                    }
                    else
                    {
                        str.Add("fizz");
                    }
                }
                else if (i % 5 == 0)
                {
                    if (day.Equals("Wednesday"))
                    { str.Add("wuzz"); }
                    else
                    str.Add("buzz");
                }
                else
                    str.Add(i.ToString());
            }

            return str;
        }

        public string getdayofweek()
        {
            return DateTime.Now.DayOfWeek.ToString();
        
        }
        


    }
     
}
