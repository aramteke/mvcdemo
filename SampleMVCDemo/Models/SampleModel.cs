﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SampleMVCDemo.Models
{
    public class SampleModel
    {
        [Required(ErrorMessage = "Number is required")]
        [Display(Name = "Enter any positive number")]
        [Range(0, 1000, ErrorMessage = "The number should be postive intege between 1 and 1000")]
        public int number { get; set; }
        public string names { get; set; }
    }
}